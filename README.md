# trillion_assignment_client

## Project setup
```
npm install
```

## Api configuration
```
You can configure the rest_url of the backend server
inside the file src/assets/js/Constants.js
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
