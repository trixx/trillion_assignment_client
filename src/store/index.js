import Vue from 'vue'
import Vuex from 'vuex'
import ArticlesModule from './modules/articlesModule';
import SelectedArticleModule from './modules/selectedArticleModule';

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    ArticlesModule,
    SelectedArticleModule
  }
})
