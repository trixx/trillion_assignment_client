import { Constants } from '../../assets/js/Constants.js';

const state = {
    similarArticles: []
};

const getters = {
    getSimilarArticles: state => state.similarArticles
};

const actions = {

    async fetchArticle({ commit }, payload) {
        try {
            let response = await fetch(`${Constants.REST_API}/articles/${payload.slug}`, {
                method: 'GET'
            });
            if (response && response.ok) {
                let data = await response.json();
                // empty the similar Articles when fetching new Article
                // as similar articles where of the previous fetched
                commit('setSimilarArticles', []);
                return data;
            } else {
                console.log("error during fetching article");
                let data = await response.json();
                commit('setSimilarArticles', []);
                return data;
            }
        } catch (error) {
            console.log("exception during fetching article");
            let errorResponse = {};
            errorResponse.error = error.message;
            commit('setSimilarArticles', []);
            return errorResponse;
        }
    },

    async fetchSimilarArticles({ commit }, payload) {
        let { page, resultsPerPage, concat } = payload;
        try {
            let response = await fetch(`${Constants.REST_API}/articles/similar/${payload.id}?page=${page}&resultsPerPage=${resultsPerPage}`, {
                method: 'GET'
            });
            if (response && response.ok) {
                let data = await response.json();
                concat ? commit('addSimilarArticles', data) : commit('setSimilarArticles', data);
                return data;
            } else {
                console.log("error during fetching similar articles");
                let data = await response.json();
                return data;
            }
        } catch (error) {
            console.log("exception during fetching similar articles");
            let errorResponse = {};
            errorResponse.error = error.message;
            return errorResponse;
        }
    },
};

const mutations = {
    setSimilarArticles: (state, similarArticles) => (state.similarArticles = similarArticles),
    addSimilarArticles: (state, newSimilarArticles) => (state.similarArticles = state.similarArticles.concat(newSimilarArticles))
};

export default {
    state,
    getters,
    actions,
    mutations
};