import { Constants } from '../../assets/js/Constants.js';

const state = {
    articles: [],
    totalArticles: 0
};

const getters = {
    getArticles: state => state.articles,
    getTotalArticles: state => state.totalArticles
};

const actions = {
    async fetchArticles({ commit }, payload) {
        let { page, resultsPerPage } = payload;
        try {
            let response = await fetch(`${Constants.REST_API}/articles?page=${page}&resultsPerPage=${resultsPerPage}`, {
                method: 'GET'
            });
            if (response && response.ok) {
                let data = await response.json();
                commit('setArticles', data[0]);
                commit('setTotalArticles', data[1]);
                return data;
            } else {
                console.log("error during fetching articles");
                let data = await response.json();
                return data;
            }
        } catch (error) {
            console.log("exception during fetching articles");
            let errorResponse = {};
            errorResponse.error = error.message;
            return errorResponse;
        }
    }
};

const mutations = {
    setArticles: (state, articles) => (state.articles = articles),
    setTotalArticles: (state, totalArticles) => (state.totalArticles = totalArticles)
};

export default {
    state,
    getters,
    actions,
    mutations
};