import Vue from 'vue';
import VueRouter from 'vue-router';
import Articles from '../views/Articles.vue';
import SelectedArticle from '../views/SelectedArticle.vue';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Articles',
    component: Articles
  },
  {
    path: "/article/:slug",
    name: "article",
    component: SelectedArticle,
    props: (route) => ({
      ...route.params
    })
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
